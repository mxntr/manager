
import {Provider} from "react-redux"
import {  createStore, applyMiddleware } from "redux";
import React, {Component} from 'react';

import reducers from "./src/reducers";
import firebase from "firebase"
import LoginForm from "./src/components/LoginForm";
import ReduxThunk from "redux-thunk";
import Router from "./src/Router"



export default class App extends Component {
  componentWillMount(){
    var config = {
      apiKey: "AIzaSyD4Poaz0dyrurxdeOJMGGK0zVgoNOyn7AQ",
      authDomain: "manager-cd7a2.firebaseapp.com",
      databaseURL: "https://manager-cd7a2.firebaseio.com",
      projectId: "manager-cd7a2",
      storageBucket: "manager-cd7a2.appspot.com",
      messagingSenderId: "201358267518"
    };
    firebase.initializeApp(config);
  };
  render() {

    return (
      <Provider store = {createStore(reducers,{},applyMiddleware(ReduxThunk))}>
        <Router />
      </Provider>
    );
  }
}



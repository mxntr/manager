import React from "react";
import { Scene, Router, Actions } from "react-native-router-flux";
import LoginForm from "./components/LoginForm";
import WishList from "./components/WishList"
import ExploreScreen from "./components/ExploreScreen"
import Icon from "react-native-vector-icons/FontAwesome"
import SearchScreen from "./components/SearchScreen"
import Cart from "./components/Cart"
import SignUpForm from "./components/SignUpForm";

const tabIcon = iconName =>{
    return (<Icon name={iconName} size={30}/>)
}

const RouterComponent = () => {
    return (
        <Router>
            <Scene key = "root" hideNavBar >
                <Scene key="tabbar" 
                tabs
                tabBarStyle = {{backgroundColor:"#FFFFFF"}}>
                    <Scene key = "main" title="MAIN"
                        icon={()=><Icon name="home" size={30}/>}
                        iconName="home"
                        hideNavBar initial>
                            <Scene key="exploreScreen"
                            component={ExploreScreen}
                            title = "Categories"
                            initial/>
                    </Scene>  
                    <Scene key = "search" title="SEARCH"
                        icon={()=><Icon name="search" size={30}/>}
                        iconName="search"
                        component={SearchScreen}
                        />
                    <Scene key = "wishList" title="WISHLIST"
                        icon={()=><Icon name="heart" size={30}/>}
                        iconName="heart"
                        component={WishList}
                        />        
                    <Scene key = "cart" title="CART"
                        icon={()=><Icon name="shopping-bag" size={30}/>}
                        iconName="shopping-bag"
                        component={Cart}
                        />   

                    <Scene key="auth" title="ACCOUNT" 
                        icon={()=><Icon name="user" size={30}/>}>
                        <Scene key="login"
                        component={LoginForm}
                        title = "Please Login"
                        rightTitle= "Sign Up"
                        onRight={()=>Actions.signUp()}
                        />
                        <Scene key="signUp"
                        component={SignUpForm}
                        title = "Please Sign Up"
                        
                        />
                    </Scene>
                     

                </Scene>

            </Scene>
        </Router>
    );
};

export default RouterComponent;
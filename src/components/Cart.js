import React, { Component } from 'react';
import { View, Text , Button} from 'react-native';
import {Input, Card, CardSection } from "./common" 

export default class WishList extends Component {
  

  render() {
    return (
      <Card>
          <CardSection>
              <Text>Your Cart is Empty</Text>
              
          </CardSection>
          <Button title="START" onPress={()=>alert("Add something")}/>
      </Card>
    );
  }
}
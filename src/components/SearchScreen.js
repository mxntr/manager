import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Input, Card, CardSection } from "./common" 
import Icon from "react-native-vector-icons/FontAwesome"

export default class SearchScreen extends Component {
 

  render() {
    return (
      <Card>
          <CardSection>
              <Input placeholder="Enter search terms" label=""/>
              <Icon name="search" size={25} color="grey"/>
          </CardSection>
      </Card>
    );
  }
}

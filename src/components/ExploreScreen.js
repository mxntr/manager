import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  TextInput,
  Dimensions,
  Animated
} from 'react-native';

import Icon from "react-native-vector-icons/Ionicons"
import Category from "./common/Category"
import Tag from "./common/Tag"




const {height,width} = Dimensions.get("window");
export default class HomeScreen extends React.Component {
  

  
  

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{flex:1}}>
          <View style={{backgroundColor: "white",
           borderBottomWidth:1, borderBottomColor:"#dddddd"}}>
          
          <View style={{flexDirection: 'row', padding:10, backgroundColor: "white", marginHorizontal:20,
              shadowOffset:{width:0, height:0}, shadowColor:"black", shadowOpacity:0.2, elevation:1,
              }}>
            
            
          </View>
          
          
          </View>
          <ScrollView 
            scrollEventThrottle = {16}>
              <View style = {styles.scrollInView}>
                
                <View style={{height:130, marginTop: 10, backgroundColor:"white", paddingLeft:5 }}>
                  <ScrollView
                    horizontal = {true}
                    showsHorizontalScrollIndicator = {false}>
                    <Category name= "Home" imageUri ={require("../assets/images/artistic.jpg")}/>
                    <Category name= "Experiences" imageUri ={require("../assets/images/bench.jpg")}/>
                    <Category name= "Restaurant" imageUri ={require("../assets/images/clouds.jpg")}/>
                    <Category name= "Hotels" imageUri ={require("../assets/images/fall.jpg")}/>
                  </ScrollView>
                </View>
                <View style ={{marginTop: 10, paddingHorizontal : 10}}>
                  
                  <View style={{width : width-20, height:200, marginTop: 10}}>
                    <Image style = {{flex:1, height:null, width: null,
                    resizeMode:"cover", borderRadius:5, borderWidth:1,
                  borderColor: "#dddddd"}}
                    source = {require("../assets/images/nwsd.jpeg")}/>

                    </View>
                    <View style={{width : width-20, height:200}}>
                    <Image style = {{flex:1, height:null, width: null,
                    resizeMode:"cover", borderRadius:5, borderWidth:1,
                  borderColor: "#dddddd"}}
                    source = {require("../assets/images/artistic.jpg")}/>

                    </View>
                </View>
              </View>

          </ScrollView>
        
        </View>
     </SafeAreaView>
    );
  }
};

 



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
   
  },
  scrollText:{
    fontSize:24,
    fontWeight:"700",
    paddingHorizontal: 20
  },
  scrollInView:{
    flex:1,
    backgroundColor: "white",
    paddingTop:20
  }
  
});

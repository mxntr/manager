import React, { Component } from 'react';
import { Card, CardSection, Input} from "./common";
import {Button, Picker, Text, StyleSheet} from "react-native";
import {connect} from "react-redux"
import {employeeUpdate} from "../actions"

class EmployeeCreate extends Component {
  

  render() {
    return (
      <Card>
        <CardSection>
          <Input label="Name"
          placeholder = "Jane"
          value = {this.props.name}
          onChangeText = {text=>this.props.employeeUpdate({prop:"name",value:text })}
          />
        </CardSection>
        <CardSection>
          <Input label="Phone"
          placeholder = "543909223339"
          value = {this.props.phone}
          onChangeText = {text=>this.props.employeeUpdate({prop:"phone",value:text })}
          />
        </CardSection>
        <CardSection style= {{flexDirection: 'column'}}>
          <Text style = {styles.pickerLabelStyle}>Shift</Text>
          <Picker 
          
          selectedValue={this.props.shift}
          onValueChange= {day=>this.props.employeeUpdate({prop:"shift", value:day})}>
            <Picker.Item label= "Monday" value="M"/>
            <Picker.Item label= "Tuesday" value="Tue"/>
            <Picker.Item label= "Wednesday" value="W"/>
            <Picker.Item label= "Thursday" value="Th"/>
            <Picker.Item label= "Friday" value="F"/>
            <Picker.Item label= "Saturday" value="St"/>
            <Picker.Item label= "Sunday" value="Sn"/>
          </Picker>
        </CardSection>
        <CardSection>
            <Button title = "create"/>
        </CardSection>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
 pickerLabelStyle:{
  fontSize:18,
  paddingLeft: 24,
 }
})

const mapStateToProps = (state) =>{
  const {name,phone,shift} = state.employeeForm;
  
  return {name,phone,shift}
};

export default connect(mapStateToProps,{employeeUpdate})(EmployeeCreate)

import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';




export default class Tag extends React.Component {
  

  render() {
    return (
        <View style ={{minWidth:40, minHeight:20, padding:5,
            backgroundColor: "white", borderColor:"#dddddd", borderWidth:0.2,
            borderRadius:2, marginRight:5}}>
              <Text style = {{fontWeight:"700", fontSize:10}}>
                {this.props.tagName}
              </Text>
            </View>
    );
  }
};

 





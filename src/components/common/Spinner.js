import React from 'react';
import { Text, View, ActivityIndicator, StyleSheet } from 'react-native';

const Spinner = () => (
    <View style = {styles.spinnerStyle}>
        <ActivityIndicator />
    </View>
);

const styles = StyleSheet.create({
    spinnerStyle:{
        flex:1,
        justifyContent:"center",
        alignItems: 'center',
    }
})

export {Spinner};

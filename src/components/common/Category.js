import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';




export default class Category extends React.Component {
  

  render() {
    return (
        <View style = {{height:120, width:110,
           borderColor:"#dddddd"}}>
              <View style = {{height:100, width:100, borderRadius:50}}>
                <Image 
                source = {this.props.imageUri}
                style={{flex:1, height:100, width:100, borderRadius:50}}/>
              </View>
              <View style = {{ paddingTop:5,paddingRight:10,alignSelf:"center"}}>
                <Text>{this.props.name}</Text>
              </View>
          </View>
    );
  }
};

 





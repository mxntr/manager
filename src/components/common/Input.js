import React, { Component } from 'react';
import { View, Text , TextInput, StyleSheet} from 'react-native';

class Input extends Component {
  

  render() {
    return (
      <View style={styles.inputContainerStyle}>
        <Text style = {styles.labelStyle}> {this.props.label} </Text>
        <TextInput 
        autoCorrect = {false}
        style = {styles.textInputStyle} 
        placeholder = {this.props.placeholder}
        onChangeText={this.props.onChangeText}
        value = {this.props.value}
        secureTextEntry={this.props.secureTextEntry}/>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
    inputContainerStyle:{
        height:40,
        flex:1,
        flexDirection:"row",
        alignItems: 'center',
    },
    labelStyle:{
        fontSize:18,
        paddingLeft:20,
        flex:1

    },
    textInputStyle:{
        color:"#000",
        paddingRight: 5,
        paddingLeft:5, 
        fontSize:18,
        lineHeight:23,
        flex:2,
        
    }
});

export {Input}
